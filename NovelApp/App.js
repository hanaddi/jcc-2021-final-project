import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { useFonts } from 'expo-font';
import Router from './Router';

export default function App() {
  return (
    <Router />
  );
}

