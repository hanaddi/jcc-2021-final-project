import { configureStore, createSlice } from '@reduxjs/toolkit' 

	
export const Store = createSlice({
	name: "sesi",
	initialState: {
		user: null,
		refreshHome: false,
		refreshLibrary: false,
	},
	reducers: {
		setRefreshHome: (state, action) => {
			state.refreshHome = action.payload;
		},
		setRefreshLibrary: (state, action) => {
			state.refreshLibrary = action.payload;
		},
		setUser: (state, action) => {
			// console.log('action ', action);
			state.user = action.payload;
		},

		removeUser: (state) => {
			state.user = null;
		},

		logout: (state) => {
			state.user = null;
		},
		test: (state, action) => {
			console.log('action ', action);
		},
	},
});

export const store = configureStore({ reducer: {
	sesi : Store.reducer
},});