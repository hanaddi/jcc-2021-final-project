import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button, ImageBackground } from 'react-native';
import Style, {El} from '../Style';

import {store, Store} from '../Store';
import { useDispatch } from 'react-redux';

export default function FrontScreen({navigation}) {
  return (
  	<ImageBackground style={Style.mainContainer} source={require('../assets/bg1.png')} resizeMode="cover" >
  		<View style={Style.centeredView}>
			<ImageBackground source={require('../assets/logo_name_black.png')} resizeMode="contain" style={{width:'100%', height:70}}>
			</ImageBackground>
  		</View>
  		<View style={[Style.centeredView,{flex:2}]}>
			<El.button text='Masuk' onPress={()=>navigation.navigate('Login')} />
			<El.button text='Daftar' onPress={()=>navigation.navigate('Register')} />
  		</View>
    </ImageBackground>
  );
}

