import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react';
import { StyleSheet, Text, View, Button, ImageBackground, SafeAreaView, FlatList, TouchableOpacity, TextInput } from 'react-native';
import axios from 'axios';
import { AntDesign } from '@expo/vector-icons'; 
import { useSelector, useDispatch } from 'react-redux';

import Style, {El, xColor} from '../Style';
import {store, Store} from '../Store';

export default function SearchScreen({route, navigation}) {
	const dispatch = useDispatch();
	const sesi = useSelector(state => state.sesi);
	const [errorText, setErrorText] = useState('');
	const [bookSearch, setBookSearch] = useState([]);
	const [newSearchText, setNewSearchText] = useState([]);
	const [loadingVisible, setLoadingVisible] = useState(false);
	const {searchText} = route.params;


	const getNovel = (query)=>{

		query = encodeURIComponent(query);
		axios.get(`https://api-buku.online.masuk.id/search?q=${query}`)
		.then(res=>{
			if(res.data.success=='true'){
				return res.data.data;
			}else{
				throw (res.data.error);
			}
		})
		.then(res=>{
			console.log("SEARH RESULT",res);

			setBookSearch(res.map(el=>(
				<El.bookDisplay title={el.title} cover={el.cover} key={el.book_id} onPress={()=>navigation.navigate('Novel',{
					book_id:el.book_id,
					title:el.title,
				})} />
			)));
		})
		.catch(err=>{
			console.log('error ', err);
			setErrorText(err || "Terjadi kesalahan");
		})
		.finally(()=>{
			setLoadingVisible(false);
		})
	};

	const doSearch =()=>{
		if(newSearchText.length>0){
			setLoadingVisible(true);
			getNovel(newSearchText);
		}
	};


	useEffect(()=>{
		getNovel(searchText);
		setNewSearchText(searchText);
	}, [searchText]);


	return (
		<View style={Style.mainAppContainer}>
	  		<El.modalLoading visible={loadingVisible} />
			<SafeAreaView style={{flex: 1}}>
				<View style={Style.mainAppContainer}>

					<View style={Style.searchContainer}>
						<View style={Style.searchBar}>
							<TextInput style={Style.inputField} placeholder='Cari judul novel' placeholderTextColor={xColor.blue_gray}  onChangeText={(value)=>setNewSearchText(value)} returnKeyType='search' onSubmitEditing={doSearch} />
						</View>
						<TouchableOpacity style={Style.buttonSearch} onPress={doSearch}>
							<Text style={Style.buttonSearchText}>cari</Text>
						</TouchableOpacity>
					</View>

					<El.error text={errorText} />

					<View style={Style.profilePanel}>
						<El.bookListWide title={`Hasil pencarian untuk "${newSearchText}"`}>
							{bookSearch.length>0 && bookSearch}
						</El.bookListWide>
					</View>


				</View>
			</SafeAreaView>
		</View>
	);

}

