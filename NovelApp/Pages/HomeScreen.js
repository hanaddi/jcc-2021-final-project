import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react';
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, ScrollView, TextInput } from 'react-native';
import Style, {El, xColor} from '../Style';
import axios from 'axios';

import {store, Store} from '../Store';
import { useSelector, useDispatch } from 'react-redux';

export default function HomeScreen({navigation}) {
	const dispatch = useDispatch();
	const sesi = useSelector(state => state.sesi);

	const [feed, setFeed] = useState(-1);
	const [bookNew, setBookNew] = useState([]);
	const [searchText, setSearchText] = useState('');
	const [bookFavorite, setBookFavorite] = useState([]);
	const [bookNewChapter, setBookNewChapter] = useState([]);

	const getFeed = ()=>{
		axios.get('https://api-buku.online.masuk.id/feed')
		.then(res=>{
			if(res.data.success=='true'){
				return res.data.data;
			}else{
				throw (res.data.error);
			}
		})
		.then(res=>{
			console.log('success');
			// console.log(res.data.data);
			setFeed(res);
		})
		.catch(err=>{
			
			console.log('error ', err);
		})
		.finally(()=>{
			dispatch(Store.actions.setRefreshHome(false));
		})

	};

	const doRefreshPage = ()=>{
		setFeed(-1);
	};

	const doSearch =()=>{
		if(searchText.length>0){
			navigation.navigate('Search',{
				searchText,
				title: 'Pencarian',
			})
		}
	};

	useEffect(()=>{
		if(sesi.refreshHome){
			doRefreshPage();
		}
	},[sesi.refreshHome]);

	useEffect(()=>{
		console.log('feed changed ', feed);
		if(feed === -1){
			getFeed();
		}else
		if(feed == null){

		}else
		if(feed){
			setBookNew(feed.new.map(el=>(
				<El.bookDisplay title={el.title} cover={el.cover} key={el.book_id} onPress={()=>navigation.navigate('Novel',{
					book_id:el.book_id,
					title:el.title,
					// refresh: doRefreshPage,
				})} />
			)));

			setBookFavorite(feed.favorite.map(el=>(
				<El.bookDisplay title={el.title} cover={el.cover} key={el.book_id} loves={parseInt(el.loves)} onPress={()=>navigation.navigate('Novel',{
					book_id:el.book_id,
					title:el.title,
					// refresh: doRefreshPage,
				})} />
			)));

			setBookNewChapter(feed.new_chapter.map(el=>(
				<El.bookDisplay title={el.title} cover={el.cover} key={el.book_id} onPress={()=>navigation.navigate('Novel',{
					book_id:el.book_id,
					title:el.title,
					// refresh: doRefreshPage,
				})} />
			)));

		}

	},[feed]);

	// useEffect(()=>{
	// 	console.log('bookNew changed ', bookNew);

	// },[bookNew]);

	return (
		<View style={Style.mainContainer}>
			<SafeAreaView style={{flex: 1}}>
				<ScrollView style={Style.mainAppContainer}>

					{/*Search Bar*/}
					<View style={Style.searchContainer}>
						<View style={Style.searchBar}>
							<TextInput style={Style.inputField} placeholder='Cari judul novel' placeholderTextColor={xColor.blue_gray}  onChangeText={(value)=>setSearchText(value)} returnKeyType='search' onSubmitEditing={doSearch} />
						</View>
						<TouchableOpacity style={Style.buttonSearch} onPress={doSearch}>
							<Text style={Style.buttonSearchText}>cari</Text>
						</TouchableOpacity>
					</View>

					<El.bookList title='Novel Baru'>
						{bookNew.length>0 && bookNew}
					</El.bookList>

					<El.bookList title='Terpopuler'>
						{bookFavorite.length>0 && bookFavorite}
					</El.bookList>

					<View style={Style.profilePanel}>
						<El.bookListLarge title='Chapter Baru'>
							{bookNewChapter.length>0 ? bookNewChapter : [(<El.bookDisplayDummy key={1} />),(<El.bookDisplayDummy key={2} />)] }
						</El.bookListLarge>
					</View>



				</ScrollView>
			</SafeAreaView>
		</View>
	);
}

