import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react';
import { StyleSheet, Text, View, Button, ImageBackground, SafeAreaView } from 'react-native';
import Style, {El} from '../Style';

import {store, Store} from '../Store';
import { useSelector, useDispatch } from 'react-redux';

export default function ProfileScreen({navigation}) {
	const dispatch = useDispatch();
	const sesi = useSelector(state => state.sesi);

	let avatar = '../assets/icons/user.png';

	console.log(sesi);
	const doLogout=()=>{
		dispatch(Store.actions.logout());
	};
	return (
		<View style={Style.mainContainer}>
			<View style={Style.mainAppContainer}>
				<SafeAreaView style={{flex: 1}}>
					<View style={Style.profilePanel}>
					<Text style={Style.title1}>Profilku</Text>

					{sesi.user && sesi.user.avatar ? (
							<ImageBackground style={[Style.bookCover]} source={{uri:sesi.user.avatar}} resizeMode="cover" />
						):(
							<ImageBackground style={[Style.bookCover]} source={require(avatar)} resizeMode="cover" />
						)
					}
					
					<El.tableRow text1='Nama' text2={sesi.user.name} />
					<El.tableRow text1='Email' text2={`${sesi.user.email} [${parseInt(sesi.user.is_verified)==0 && 'belum '}terverifikasi]` } />

					<El.button text='Keluar' style='second' onPress={doLogout} />


					</View>
				</SafeAreaView>
			</View>
		</View>
	);
}

