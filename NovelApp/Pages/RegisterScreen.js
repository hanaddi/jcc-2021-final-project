import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react';
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, ImageBackground } from 'react-native';
import Style, {El, xColor} from '../Style';
import axios from 'axios';

import {store, Store} from '../Store';
import { useDispatch } from 'react-redux';

export default function RegisterScreen({navigation}) {
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [pass, setPass] = useState('');
	const [repass, setRepass] = useState('');

	const [goTo, setGoTo] = useState('');
	const [loadingVisible, setLoadingVisible] = useState(false);
	const [errorText, setErrorText] = useState('');

	useEffect(()=>{
		if(goTo && goTo!=''){
			navigation.navigate(goTo);
		}
	},[goTo]);
	
	const doRegister = ()=>{
		let data ={name, email, pass, repass};
		console.log(data);
		setLoadingVisible(true);

		axios.post('https://api-buku.online.masuk.id/register', data)
		.then(res=>{
			if(res.data.success=='true'){
				return res.data.data;
			}else{
				throw (res.data.error);
			}
		})
		.then(res=>{
			setErrorText('');
			setGoTo('Login');
		})
		.catch(err=>{
			setErrorText(err || 'Terjadi kesalahan');
		})
		.finally(()=>{
			setLoadingVisible(false);
		})

	};
	return (
		<View style={Style.mainContainer}>
			<El.modalLoading visible={loadingVisible} />
			<View style={Style.loginLogoContainer}>
				<ImageBackground source={require('../assets/logo_name_white.png')} resizeMode="contain" style={{width:'100%', height:70}}>
				</ImageBackground>
			</View>
			<View style={Style.loginContainer}>
				<SafeAreaView style={{flex: 1}}>
					<View style={Style.loginPanel}>
						<Text style={Style.title1}>Buat akun</Text>
						<El.error text={errorText} />
						<El.formField title='Nama' icon={require('../assets/icons/user.png')} onChangeText={(value)=>setName(value)} />
						<El.formField title='Email' icon={require('../assets/icons/mail.png')} onChangeText={(value)=>setEmail(value)} />
						{/*{<El.button text='test' onPress={()=>console.log('test press')} />}*/}
						<El.formField title='Password' icon={require('../assets/icons/lock.png')} secureTextEntry onChangeText={(value)=>setPass(value)}/>
						<El.formField title='Ulangi password' icon={require('../assets/icons/lock.png')} secureTextEntry onChangeText={(value)=>setRepass(value)}/>
						<El.button text='Daftar' onPress={doRegister} />

						<View style={{alignItems: 'center', marginTop: 50}}>
							<TouchableOpacity onPress={() => navigation.navigate('Login')}>
								<Text>Sudah punya akun? <Text style={{fontWeight:'bold', color:xColor.blue_grotto}}>Masuk</Text></Text>
							</TouchableOpacity>
						</View>

					</View>
				</SafeAreaView>
			</View>
		</View>
  );
}

