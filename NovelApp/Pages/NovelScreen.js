import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react';
import { StyleSheet, Text, View, Button, ImageBackground, SafeAreaView, ScrollView, FlatList } from 'react-native';
import axios from 'axios';
import { AntDesign } from '@expo/vector-icons'; 
import { useSelector, useDispatch } from 'react-redux';

import Style, {El} from '../Style';
import {store, Store} from '../Store';

export default function NovelScreen({route, navigation}) {
	const dispatch = useDispatch();
	const sesi = useSelector(state => state.sesi);
	const [book, setBook] = useState([]);
	const [errorText, setErrorText] = useState('');
	const [chapterList, setChapterList] = useState([]);

	let avatar = '../assets/logo_white.png';

	const {book_id} = route.params;

	const getBook = (data)=>{
		// let data = {
		// 	token: sesi.user.token,
		// };
		axios.post(`https://api-buku.online.masuk.id/novel/${book_id}`, data)
		.then(res=>{
			if(res.data.success=='true'){
				return res.data.data;
			}else{
				throw (res.data.error);
			}
		})
		.then(res=>{
			console.log(res);
			setBook(res);

			// test long list
			// setBook({
			// 	...res,
			// 	chapters: [...res.chapters, ...res.chapters, ...res.chapters, ...res.chapters, ...res.chapters, ...res.chapters, ...res.chapters],
			// });
		})
		.catch(err=>{
			console.log('error ', err);
			setErrorText(err || "Terjadi kesalahan");
		})
		.finally(()=>{
		})
	};
	const toggleLike = ()=>{
		if(book.logged_in && parseInt(book.logged_in)){
			dispatch(Store.actions.setRefreshHome(true));
			dispatch(Store.actions.setRefreshLibrary(true));
			let like = !parseInt(book.loved)?'1':'0';
			// console.log('like ', like);
			return getBook({
				token: sesi.user.token,
				like,
			});
		}
	};

	const scrollCallback = ({nativeEvent}) =>{
		const {layoutMeasurement, contentOffset, contentSize} = nativeEvent;
		let padding = 10
		if(layoutMeasurement.height + contentOffset.y >= contentSize.height - padding){
			if(chapterList.length<book.chapters.length){
				setChapterList([
					...chapterList,
					...book.chapters.slice(chapterList.length, chapterList.length+3),
				]);
			}
		}
	};
	useEffect(()=>{
		getBook({token: sesi.user.token});
	},[]);


	useEffect(()=>{
		if(book.chapters){
			setChapterList(book.chapters.slice(0,30));
		}
	},[book]);

	return (
		<View style={Style.mainAppContainer}>
			<SafeAreaView style={{flex: 1}}>
				<ScrollView style={Style.mainAppContainer} onScroll={scrollCallback}>
					<View style={Style.profilePanel}>
						<El.error text={errorText} />

						{book.cover ? (
								<ImageBackground style={[Style.bookCover, {backgroundColor: 'transparent'}]} source={{uri:book.cover}} resizeMode="contain" />
							):(
								<ImageBackground style={[Style.bookCover,]} source={require(avatar)} resizeMode="contain" />
							)
						}
						{book.loved && book.logged_in && parseInt(book.logged_in) && (parseInt(book.loved) ?(
								<El.button text={(<><AntDesign name="heart" size={14} /><Text> Disukai</Text></>)} style='liked' onPress={toggleLike} />
							):(
								<El.button text={(<><AntDesign name="hearto" size={14} /><Text> Suka</Text></>)} style='like' onPress={toggleLike} />
							)
						)}

						{book.title && (<El.tableRow text1='Judul' text2={book.title} />)}
						{book.writer && (<El.tableRow text1='Penulis' text2={book.writer} />)}
						{book.loves && (<View style={Style.tableRow}><Text style={Style.tableCell}>Disukai oleh {parseInt(book.loves)} orang</Text></View>)}
						{book.summary && (<Text style={Style.summaryContainer}>{book.summary}</Text>)}


					</View>
					<View style={Style.profilePanel}>
						<Text style={Style.title3}>Chapter</Text>

						{/*FlatList di dalam ScrollView*/}
						{false && book.chapters && (
							book.chapters.length>0?(
									<FlatList style={{height: Math.min(350, Math.max(60, book.chapters.length*47)), flex: 1}}
										nestedScrollEnabled
										data={book.chapters}
										keyExtractor={(item, index)=> `${item.chapter_id}-${item.number}-${index}`}
										renderItem={({item})=>{
												// <View style={Style.tableRow}><Text style={Style.tableCell}>Chapter {item.number} {item.title} </Text></View>
											return (
												<El.chapterList text={`Chapter ${item.number} ${item.title}`} onPress={()=>navigation.navigate('Chapter',{
													// title: `Chapter ${item.number} ${item.title}`,
													title: book.title,
													book_id: book.book_id,
													chapter_id: item.chapter_id,
												})} />
											);
										}}
									/>
								):(
									<Text style={Style.tableCell}>Belum ada chapter</Text>
								)
						)}

						{chapterList.length>0 ? (
							chapterList.map((item, i)=>(
								<El.chapterList key={i} text={`Chapter ${item.number} ${item.title}`} onPress={()=>navigation.navigate('Chapter',{
									title: book.title,
									book_id: book.book_id,
									chapter_id: item.chapter_id,
								})} />
							))
							):(
								book.chapters && (<Text style={Style.tableCell}>Belum ada chapter</Text>)
							)
						}

					</View>
				</ScrollView>
			</SafeAreaView>
		</View>
	);
}

