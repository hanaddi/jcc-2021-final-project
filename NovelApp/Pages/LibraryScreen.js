import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react';
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, ScrollView } from 'react-native';
import Style, {El, xColor} from '../Style';
import axios from 'axios';

import {store, Store} from '../Store';
import { useSelector, useDispatch } from 'react-redux';

export default function LibraryScreen({navigation}) {
	const dispatch = useDispatch();
	const sesi = useSelector(state => state.sesi);

	const [library, setLibrary] = useState(-1);
	const [bookLiked, setBookliked] = useState([]);
	const [bookReadHistory, setBookReadHistory] = useState([]);

	const getLibrary = ()=>{
		let data = {
			token: sesi.user.token
		};
		axios.post('https://api-buku.online.masuk.id/library', data)
		.then(res=>{
			if(res.data.success=='true'){
				return res.data.data;
			}else{
				throw (res.data.error);
			}
		})
		.then(res=>{
			// console.log('success');
			// console.log(res);
			setLibrary(res);
		})
		.catch(err=>{
			
			console.log('error ', err);
		})
		.finally(()=>{
			dispatch(Store.actions.setRefreshLibrary(false));
		})

	};

	const doRefreshPage = ()=>{
		setLibrary(-1);
	};

	useEffect(()=>{
		if(sesi.refreshLibrary){
			console.log("REFRESH PAGE");
			doRefreshPage();
		}
	},[sesi.refreshLibrary]);


	useEffect(()=>{
		if(!sesi.user){
			dispatch(Store.actions.logout());
		}
	},[sesi.user]);

	useEffect(()=>{
		console.log('library changed ', library);
		if(library === -1){
			getLibrary();
		}else
		if(library == null){

		}else
		if(library){
			setBookliked(library.liked.map(el=>(
				<El.bookDisplay title={el.title} cover={el.cover} key={el.book_id} onPress={()=>navigation.navigate('Novel',{
					book_id:el.book_id,
					title:el.title,
				})} />
			)));

			setBookReadHistory(library.history.map(el=>(
				<El.bookDisplay title={el.title} cover={el.cover} key={el.book_id} onPress={()=>navigation.navigate('Novel',{
					book_id:el.book_id,
					title:el.title,
					// refresh: doRefreshPage,
				})} />
			)));


		}

	},[library]);



	return (
		<View style={Style.mainContainer}>
			<SafeAreaView style={{flex: 1}}>
				<ScrollView style={Style.mainAppContainer}>


					<View style={Style.profilePanel}>
						<El.bookListLarge title='Disukai'>
							{bookLiked.length>0 ? bookLiked : [(<El.bookDisplayDummy key={1} />),(<El.bookDisplayDummy key={2} />)] }
						</El.bookListLarge>
					</View>

					<View style={Style.profilePanel}>
						<El.bookListLarge title='Terakhir Dibaca'>
							{bookReadHistory.length>0 ? bookReadHistory : [(<El.bookDisplayDummy key={1} />),(<El.bookDisplayDummy key={2} />)] }
						</El.bookListLarge>
					</View>


				</ScrollView>
			</SafeAreaView>
		</View>
	);
}

