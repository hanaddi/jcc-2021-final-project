import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState, useRef} from 'react';
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, ImageBackground } from 'react-native';
import Style, {El, xColor} from '../Style';
import axios from 'axios';

import {store, Store} from '../Store';
import { useSelector, useDispatch } from 'react-redux';

export default function LoginScreen({navigation}) {
	const dispatch = useDispatch();
	const sesi = useSelector(state => state.sesi);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [user, setUser] = useState(null);
	const [goTo, setGoTo] = useState('');
	const [loadingVisible, setLoadingVisible] = useState(false);
	const [errorText, setErrorText] = useState('');
	const inputEmail = useRef();

	useEffect(()=>{
		if(user){
			dispatch(Store.actions.setUser(user));
		}
		if(inputEmail.current){
			inputEmail.current.focus();
		}
	}, [user, inputEmail]);


	const doLogin = ()=>{
		let data ={email, password};
		setLoadingVisible(true);

		axios.post('https://api-buku.online.masuk.id/login', data)
		.then(res=>{
			if(res.data.success=='true'){
				return res.data.data;
			}else{
				throw (res.data.error);
			}
		})
		.then(res=>{
			// console.log('success');
			setErrorText('');
			console.log(res);
			setUser(res);
		})
		.catch(err=>{
			setErrorText(err || 'Terjadi kesalahan');
		})
		.finally(()=>{
			setLoadingVisible(false);
		})

	};

	return (
		<View style={Style.mainContainer}>
	  		<El.modalLoading visible={loadingVisible} />
	  		<View style={Style.loginLogoContainer}>
				<ImageBackground source={require('../assets/logo_name_white.png')} resizeMode="contain" style={{width:'100%', height:70}}>
				</ImageBackground>
	  		</View>
			<View style={Style.loginContainer}>
				<SafeAreaView style={{flex: 1}}>
				  	<View style={Style.loginPanel}>
						<Text style={Style.title1}>Masuk</Text>
						<El.error text={errorText} />
						<El.formField title='Email' icon={require('../assets/icons/mail.png')} onChangeText={(value)=>setEmail(value)} />
						<El.formField title='Password' icon={require('../assets/icons/lock.png')} secureTextEntry onChangeText={(value)=>setPassword(value)}/>
						<El.button text='Masuk' onPress={doLogin} />

						<View style={{alignItems: 'center', marginTop: 50}}>
							<TouchableOpacity onPress={() => navigation.navigate('Register')}>
								<Text>Belum punya akun? <Text style={{fontWeight:'bold', color:xColor.blue_grotto}}>Mendaftar</Text></Text>
							</TouchableOpacity>
						</View>

				    </View>
				</SafeAreaView>
			</View>
		</View>
	);
}

