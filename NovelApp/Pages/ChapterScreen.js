import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react';
import { StyleSheet, Text, View, Button, ImageBackground, SafeAreaView, FlatList, TouchableOpacity } from 'react-native';
import axios from 'axios';
import { AntDesign } from '@expo/vector-icons'; 
import { useSelector, useDispatch } from 'react-redux';

import Style, {El} from '../Style';
import {store, Store} from '../Store';

export default function ChapterScreen({route, navigation}) {
	const dispatch = useDispatch();
	const sesi = useSelector(state => state.sesi);
	const [errorText, setErrorText] = useState('');
	const [chapter, setChapter] = useState({});
	const [content, setContent] = useState([]);
	const {book_id, chapter_id, title} = route.params;
	let avatar = '../assets/logo_white.png';


	const getChapter = (book_id, chapter_id, data)=>{
		// let data = {
		// 	token: sesi.user.token,
		// };
		axios.post(`https://api-buku.online.masuk.id/chapter/${book_id}/${chapter_id}`, data)
		.then(res=>{
			if(res.data.success=='true'){
				return res.data.data;
			}else{
				throw (res.data.error);
			}
		})
		.then(res=>{
			setChapter(res);
		})
		.catch(err=>{
			console.log('error ', err);
			setErrorText(err || "Terjadi kesalahan");
		})
		.finally(()=>{
			dispatch(Store.actions.setRefreshLibrary(true));
		})
	};

	const prev_chapter = ()=>{
		setChapter({
			...chapter,
			content: ["Tunggu"]
		});
		getChapter(book_id, chapter.prev_chapter.chapter_id, {token: sesi.user.token});
	};

	const next_chapter = ()=>{
		setChapter({
			...chapter,
			content: ["Tunggu"]
		});
		getChapter(book_id, chapter.next_chapter.chapter_id, {token: sesi.user.token});
	};

	// const go_novel =()=>navigation.navigate('Novel',{
	// 	book_id:book_id,
	// 	title:title,
	// });
	// const go_novel =()=>navigation.goBack();
	const go_novel =()=>navigation.navigate('MainApp');

	useEffect(()=>{
		console.log("IDS ", book_id, chapter_id);
		getChapter(book_id, chapter_id, {token: sesi.user.token});
	}, []);

	useEffect(()=>{
		console.log(chapter);
		if(chapter.content){
			setContent(Object.keys(chapter.content).map(el=>({
				id:el,
				text:chapter.content[el],
			})));
		}
	}, [chapter]);

	return (
		<View style={Style.mainAppContainer}>
			<SafeAreaView style={{flex: 1}}>
				<View style={Style.mainAppContainer}>
					{chapter.number && (<El.chapterHeader title={`Chapter ${chapter.number} ${chapter.title}`} />)}

					<View style={{flex: 1}}>
						<View style={Style.profilePanel}>
							<El.error text={errorText} />

							<FlatList 
								data={content}
								keyExtractor={(item, index)=> `${item.id}-${index}`}
								renderItem={({item})=>{
									return (
										<TouchableOpacity>
											<View style={{margin:0, padding: 8}}>
												<Text style={{textAlign: 'justify'}}>{item.text}</Text>
											</View>
										</TouchableOpacity>
									);
								}}

							/>


						</View>
					</View>
					{chapter.number && (<El.chapterFooter prev={chapter.prev_chapter} next={chapter.next_chapter} prev_func={prev_chapter} next_func={next_chapter} book_func={go_novel} />)}

				</View>
			</SafeAreaView>
		</View>
	);
}

