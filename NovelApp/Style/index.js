import { StyleSheet, Dimensions, StatusBar } from 'react-native'; 
import { useFonts } from 'expo-font';
import { Font } from 'expo';
import React, {useRef} from 'react';
import { View, Text, TextInput, Image, TouchableOpacity, Modal, ScrollView, ImageBackground, TouchableWithoutFeedback, FlatList } from 'react-native';

import { MaterialIcons, AntDesign } from '@expo/vector-icons';

const SCREEN_HEIGHT = Dimensions.get('screen').height;
const SCREEN_WIDTH = Dimensions.get('screen').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;
const WINDOW_WIDTH = Dimensions.get('window').width;
export const STATUS_BAR_HEIGHT = StatusBar.currentHeight || 30; 

export const El = {
	formField :({title, icon=null, secureTextEntry=false, onChangeText=()=>null})=>{
		let myInput = useRef();
		let setFocus = () =>{
			console.log('focus ');
			myInput.current.focus();
		};
		return (
			<TouchableWithoutFeedback onPress={setFocus}>
			<View style={Style.formField}>
				<View style={Style.inputFieldContainer}>
					<View style={Style.inputFieldIconContainer}>
						{icon && <Image source={icon} style={Style.inputFieldIcon} resizeMode="contain" />}
					</View>
					<TextInput ref={myInput} style={Style.inputField} secureTextEntry={secureTextEntry} placeholder={title} placeholderTextColor={color.blue_gray} onChangeText={onChangeText} />
				</View>
			</View>
			</TouchableWithoutFeedback>
		);
	},
	button :({text, style='main', onPress=function(){} })=>{
		let styles={
			main:{
				button: {
					backgroundColor: color.blue_grotto,
					borderColor: color.blue_grotto,
					alignSelf: 'center',
				},
				text: {
					color: color.white,
				}
			},
			second:{
				button: {
					backgroundColor: color.khaki,
					borderColor: color.blue_gray,
					alignSelf: 'center',
				},
				text: {
					color: color.maroon,
				}
			},
			like:{
				button: {
					backgroundColor: color.red,
					borderColor: color.red,
					alignSelf: 'center',
					paddingHorizontal: 10,
					paddingVertical: 6,
					borderRadius: 20,
				},
				text: {
					color: color.white,
				}
			},
			liked:{
				button: {
					backgroundColor: color.white,
					borderColor: color.red,
					alignSelf: 'center',
					paddingHorizontal: 10,
					paddingVertical: 6,
					borderRadius: 20,
				},
				text: {
					color: color.red,
				}
			}
		};
		return(
			<TouchableOpacity style={[Style.button, styles[style].button]} onPress={onPress}>
				<Text style={ [Style.buttonText, styles[style].text] }>{text}</Text>
			</TouchableOpacity>
		);
	},
	bookList :({title='', children})=>{
		return (
			<View style={Style.bookListContainer}>
				<Text style={Style.title2}>{title}</Text>
				<ScrollView horizontal style={Style.bookList} scrollEnabled={children?true:false}>
					{children? children : (
						<>
						<El.bookDisplayDummy />
						<El.bookDisplayDummy />
						<El.bookDisplayDummy />
						</>
					) }
				</ScrollView>
			</View>
		);
	},
	bookListLarge :({title='', children})=>{
		const arr = children.length%2?[...children, (<View  style={[Style.bookDisplayDummy, {opacity:0}]} />)]:children;
		let child = arr.map((e,i,a)=>i%2==0 && a.slice(i,i+2)).filter(e=>e).map((e,i)=>(<View key={i} style={{flexDirection:'row'}}>{e}</View>));
		console.log('child ', child);
		return (
			<View style={Style.bookListContainer}>
				<Text style={Style.title2}>{title}</Text>
				<View style={Style.bookListContainerWide}>
					{children? child : (
						<>
						<El.bookDisplayDummy />
						<El.bookDisplayDummy />
						<El.bookDisplayDummy />
						</>
					) }
				</View>
			</View>
		);
	},
	bookListWide :({title='', children})=>{
		return (
			<View style={Style.bookListContainer}>
				<Text style={Style.title2}>{title}</Text>
				<View style={Style.bookListContainerWide}>
					<FlatList  style={Style.bookList} scrollEnabled={children?true:false} numColumns={2}
						data=
						{children? children : [
							(<El.bookDisplayDummy key={1} />),
							(<El.bookDisplayDummy key={2} />),
						]}
						keyExtractor={(item, index)=> `${index}`}
						renderItem={({item})=>(item)}
					/>
				</View>
			</View>
		);
	},

	bookDisplayDummy :()=>(
		<View style={Style.bookDisplayDummy}>
			<View style={Style.bookCover}></View>
			<Text></Text>
		</View>
	),
	bookDisplay :({cover, title, loves=false, onPress=()=>null})=>{
		let backgroundColor = 'transparent';
		let isWriteTitle = false;
		if(!cover){
			cover="data:image/png;base64, wolQTkcNChoKAAAADUlIRFIAAAABAAAAAQgGAAAAHxXDhMKJAAAACklEQVR4AWMAAQAABQABNsOQwojDnQAAAABJRU5Ewq5CYMKC";
			backgroundColor = '#dedede';
			isWriteTitle = true;
		}
		let lovesCount = 0;
		if(loves!==false){
			if(loves>1000000){
				lovesCount = (loves/1000000|0)+'M'
			}else
			if(loves>1000){
				lovesCount = (loves/1000|0)+'K'
			}else{
				lovesCount = loves;
			}
		}
		return (
		<TouchableOpacity onPress={onPress}>
			<View style={Style.bookDisplay}>
				<ImageBackground style={[Style.bookCover, {backgroundColor}]} source={{uri:cover}} resizeMode="contain" >
					{ loves!== false && (
						<>
							<View style={Style.loveContainer}>
								<Text style={Style.loveText}>{lovesCount}</Text>
								<AntDesign name="heart" size={24} color="red" />
							</View>
						</>
					)}
					{isWriteTitle && (<Text style={{textAlign:'center', fontFamily: fontTitle, color: color.mint_blue}}>{title}</Text>)}
				</ImageBackground>
				<Text style={Style.bookDisplayTitle}>{title}</Text>
			</View>
		</TouchableOpacity>
		)
	},

	chapterHeader: ({title=''})=>(
		<View style={Style.chapterHeader}>
			<Text style={Style.title3}>{title}</Text>
		</View>
	),

	chapterFooter: ({prev=null, next=null, prev_func=()=>null, next_func=()=>null, book_func=()=>null})=>{
		return (
		<View style={Style.chapterFooter}>
			<View style={Style.chapterFooterContent}>
				{prev && (
					<TouchableOpacity onPress={prev_func}>
						<View style={Style.chapterFooterContent}>
							<MaterialIcons name="chevron-left" size={30} color="black" />
							<Text>Chaper {prev.number}</Text>
						</View>
					</TouchableOpacity>
				)}
			</View>

			<View style={Style.chapterFooterContent}>
				<TouchableOpacity onPress={book_func}>
					{/*<AntDesign name="book" size={30} color="black" />*/}
					<AntDesign name="home" size={30} color="black" />
				</TouchableOpacity>
			</View>

			<View style={Style.chapterFooterContent}>
				{next && (
					<TouchableOpacity onPress={next_func}>
						<View style={Style.chapterFooterContent}>
							<Text>Chaper {next.number}</Text>
							<MaterialIcons name="chevron-right" size={30} color="black" />
						</View>
					</TouchableOpacity>
				)}
			</View>

		</View>
	)},

	tableRow: ({text1='', text2=''})=>{
		return (
			<View style={Style.tableRow}>
				<Text style={[Style.tableCell, {flex: 1}]}>{text1}</Text>
				<Text style={[Style.tableCell]}>{text1 && text2 && ':'}</Text>
				<Text style={[Style.tableCell, {flex: 3}]}>{text2}</Text>
			</View>
		);
	},

	chapterList: ({text='', onPress=()=>null})=>{
		return (
			<TouchableOpacity onPress={onPress}>
				<View style={Style.chapterList}>
					<Text style={Style.tableCell}>{text}</Text>
				</View>
			</TouchableOpacity>
		);
	},


	error :({text=''})=>{
		text = (text || '').trim();
		return text && text.length>0?(
			<View style={Style.errorText}>
				<MaterialIcons name="error" size={24} color="red" style={{marginRight:10}} />
				<Text style={{flex: 1}} >{text}</Text>
			</View>
		):(<></>);
	},
	modalLoading:({visible=false})=>{
		return (
			<Modal
				animationType="fade"
				transparent={true}
				visible={visible}
				onRequestClose={() => {
					// Alert.alert('Modal has been closed.');
				}}>
				<View style={[Style.centeredView, {backgroundColor:'rgba(100,100,100,.6)'}]}>
					<View style={{opacity:1}}>
						<Text style={{opacity:1}}>Tunggu</Text>
					</View>
				</View>
			</Modal>
		);
	},
};

const color = {
	blue_grotto: '#2073B3',
	maroon: '#3F2D3D',
	peach: '#FFCD91',
	white: '#FAFCF6',

	salmon: '#E98973',
	khaki: '#E7D4C0',
	mint_blue: '#88B2CC',
	blue_gray: '#658EA9',

	error: '#FFBBBB',
	red: '#E7625F',
};
export const xColor = color;
const fontTitle = 'Rowdies-Bold';

const Style = StyleSheet.create({
	centeredView: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: STATUS_BAR_HEIGHT+3,
	},
	mainContainer: {
		flex:1,
		marginTop: STATUS_BAR_HEIGHT+3,
		// height: SCREEN_HEIGHT,
	},
	mainAppContainer: {
		flex:1,
		backgroundColor: color.khaki,
	},
	loginLogoContainer: {
		flex:1,
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection:'row',
		marginBottom: -10,
		backgroundColor: color.salmon,
	},
	loginContainer: {
		flex:1,
		backgroundColor: color.salmon,
		justifyContent: 'center',
		alignItems: 'flex-end',
		flexDirection:'row',
	},
	loginPanel: {
		backgroundColor: color.white,
		marginHorizontal: 20,
		paddingHorizontal: 30,
		paddingVertical: 30,
		paddingBottom: 150,
		marginBottom: -30,

		borderRadius: 30,
		borderWidth: 3,
		borderColor: color.peach,
	},

	bookListContainer: {
		margin: 5,
		marginTop: 20,
	},
	bookList: {
		padding: 5,
	},
	bookListContainerWide: {
		padding: 0,
		margin: 0,
		alignItems: 'center',
	},
	bookDisplayDummy: {
		width: 150,
		height: 200,
		backgroundColor: '#d0d0d0',
		margin: 5,
		marginHorizontal: 10,
		borderRadius: 5,
	},
	bookDisplay: {
		width: 150,
		height: 200,
		margin: 5,
		marginHorizontal: 10,
		borderRadius: 5,
		alignItems: 'center',
	},
	bookCover: {
		width: 120,
		height: 150,
		margin: 5,
		alignSelf: 'center',
		backgroundColor: '#dedede',
		borderRadius: 5,
	},


	profilePanel: {
		backgroundColor: color.white,
		margin: 20,
		padding: 20,
		paddingVertical: 10,
		borderRadius: 5,
		borderWidth: 3,
		borderColor: color.peach,
		justifyContent: 'center',
	},
	loveContainer: {
		position:'absolute',
		backgroundColor: color.white,
		padding:5,
		borderRadius: 10,
		right:0,
		alignItems:'center',
		justifyContent:'flex-end',
		zIndex:2,
		flexDirection:'row',
	},
	loveText: {
		color:color.maroon,
		fontWeight:'bold',
		marginHorizontal: 3,
	},

	tableRow: {
		flexDirection:'row',
		padding: 2,
		backgroundColor: color.peach,
		alignItems: 'flex-start',
		marginVertical: 2,
	},
	tableCell: {
		margin: 3,
		color: color.maroon,
	},
	chapterList: {
		flexDirection:'row',
		padding: 2,
		backgroundColor: color.mint_blue,
		alignItems: 'flex-start',
		marginVertical: 2,
		borderColor: color.blue_gray,
		borderWidth: 1,
		borderRadius: 3,
		marginVertical: 5,
	},
	summaryContainer: {
		padding: 10,
		color: color.maroon,
		textAlign: 'justify',
		marginVertical: 10,
	},

	chapterHeader: {
		backgroundColor: color.khaki,
		// height: 60,
		padding: 5,
		alignItems: 'center',
		marginBottom: -10,
	},

	chapterFooter: {
		backgroundColor: color.mint_blue,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		padding: 20,
	},
	chapterFooterContent: {
		flexDirection: 'row',
		alignItems:'center',
		flex: 1,
		alignSelf: 'center',
		justifyContent: 'center',
	},

	searchContainer: {
		backgroundColor: color.mint_blue,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		padding: 10,
		paddingHorizontal: 100,
	},

	searchBar: {
		borderRadius: 10,
		padding: 3,
		backgroundColor: color.white,
		borderWidth: 2,
		borderColor: color.blue_gray,
		width: '60%',
	},

	// Buttons
	button: {
		alignItems:'center',
		padding:10,
		paddingHorizontal:50,
		borderRadius: 5,
		borderWidth: 2,
		fontWeight: 'bold',
		marginVertical: 15,
	},
	buttonText:{
		fontWeight: 'bold',
		fontSize: 16,
	},

	buttonSearch: {
		alignItems:'center',
		padding:10,
		marginHorizontal: 5,
		borderRadius: 10,
		borderWidth: 1,
		fontWeight: 'bold',
		backgroundColor: color.peach,
	},
	buttonSearchText: {
		fontWeight: 'bold',
		color: color.maroon,
	},


	// Form
	formField: {
		marginHorizontal: 10,
		marginVertical: 10,
	},
	inputFieldContainer: {
		borderRadius: 5,
		padding: 3,
		overflow: 'hidden',
		flexDirection: 'row',
		backgroundColor: color.peach,
		alignItems:'center',
		borderWidth: 2,
		borderColor: color.blue_gray,
	},
	inputFieldIconContainer: {
		backgroundColor: color.blue_gray,
		height:46,
		width:40,
		margin: -3,
		marginRight:0,
		marginVertical: -6,
		justifyContent:'center',
		alignItems:'center',
	},
	inputFieldIcon: {
		width: 30,
	},
	inputField :{
		padding: 1,
		marginLeft: 5,
		flex: 1,
		color: color.maroon,
		// backgroundColor: 'red',
	},


	// Text
	title1: {
		fontFamily: fontTitle,
		fontSize: 30,
		textAlign: 'center',
		marginBottom:20,
		color: color.maroon,
	},
	title2: {
		fontFamily: fontTitle,
		fontSize: 26,
		// textAlign: 'center',
		// marginBottom:20,
		color: color.maroon,
	},
	title3: {
		fontFamily: fontTitle,
		fontSize: 20,
		// textAlign: 'center',
		// marginBottom:20,
		color: color.maroon,
	},
	errorText: {
		backgroundColor: color.error,
		marginHorizontal: 10,
		padding: 5,
		flexDirection: 'row',
		alignItems: 'center',
	},
	bookDisplayTitle: {
		textAlign: 'center',
		fontWeight: 'bold',
		color: color.blue_grotto,
	}
});


export default Style;