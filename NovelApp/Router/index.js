import { StatusBar } from 'expo-status-bar';
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import React, {useEffect, useContext} from 'react'
import { View, Text, Image, StyleSheet, SafeAreaView, FlatList, TouchableOpacity, Button } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons'; 
import { useSelector, useDispatch, Provider } from 'react-redux';
import { createStore } from 'redux';

import Style, {El, xColor} from '../Style';
import {store, Store} from '../Store';
// import screens
import FrontScreen from '../Pages/FrontScreen';
import LoginScreen from '../Pages/LoginScreen';
import RegisterScreen from '../Pages/RegisterScreen';
import HomeScreen from '../Pages/HomeScreen';
import ProfileScreen from '../Pages/ProfileScreen';
import NovelScreen from '../Pages/NovelScreen';
import ChapterScreen from '../Pages/ChapterScreen';
import LibraryScreen from '../Pages/LibraryScreen';
import SearchScreen from '../Pages/SearchScreen';


const Stack = createNativeStackNavigator();
const BottomTab = createBottomTabNavigator();

const Manager = (param) =>{
	console.log("------------------------- App -------------------------");
	const sesi = useSelector(state => state.sesi);
	const panelOptions = ({route})=>({
		animation: 'fade',
		// headerShown: false,
		title: route.params.title || 'Baca',
		headerStyle: {
			backgroundColor: xColor.white,
		},
		// headerRight: () => (
		// 	<Button
		// 		onPress={() => alert('This is a button!')}
		// 		title="Info"
		// 		color="#fff"
		// 	/>
		// ),

		headerTintColor: xColor.maroon,
		headerTitleStyle: Style.title3,
	})

	return (
		<>
		<NavigationContainer>
		{ (!sesi.user) ?
				(<Stack.Navigator initialRouteName="Front" screenOptions={{headerShown: false,  }}>
					<Stack.Screen name="Front" component={FrontScreen} />
					<Stack.Screen name="Login" component={LoginScreen} />
					<Stack.Screen name="Register" component={RegisterScreen} />
				</Stack.Navigator>)
				:
				(<Stack.Navigator initialRouteName="MainApp" screenOptions={{}}>
					<Stack.Screen name="MainApp" component={MainApp} options={{headerShown: false}} />
					<Stack.Screen name="Chapter" component={ChapterScreen} options={panelOptions} />
					<Stack.Screen name="Novel" component={NovelScreen} options={panelOptions} />
					<Stack.Screen name="Search" component={SearchScreen} options={panelOptions} />
				</Stack.Navigator>)
		}
		</NavigationContainer>
		</>
	);
};


const MainApp = () =>(
	<BottomTab.Navigator screenOptions={{
		headerShown: false,
		tabBarStyle: {
			height: 80,
		},
		tabBarItemStyle: {
			height: 70,
			padding: 10,
			// borderWidth: 2,
		},

	}}>
		<BottomTab.Screen name='Home' component={HomeScreen} options={{
			title:'Beranda',
			tabBarIcon:()=>(<MaterialCommunityIcons name="home" size={24} color="black" />),
			unmountOnBlur: true,
			// tabBarBadgeStyle: {backgroundColor:'red'},
			// tabBarBadge: 2,
			// tabBarItemStyle: {padding: 10},
		}} />
		<BottomTab.Screen name='Library' component={LibraryScreen} options={{title:'Koleksi',tabBarIcon:()=>(<MaterialCommunityIcons name="bookshelf" size={24} color="black" />), unmountOnBlur: true,}} />
		<BottomTab.Screen name='Profile' component={ProfileScreen} options={{title:'Profil', tabBarIcon:()=>(<MaterialCommunityIcons name="account" size={24} color="black" />)}} />
	</BottomTab.Navigator>
);

export default function Router (){

	let [fontLoaded] = useFonts({
		'Rowdies-Bold': require('../assets/fonts/Rowdies-Bold.ttf')
	});

	if (!fontLoaded) {
		return (<AppLoading />);
	}

	return (
		<Provider store={store}>
			<Manager />
		</Provider>

	);
};
