# <img src="https://gitlab.com/hanaddi/jcc-2021-final-project/-/raw/main/NovelApp/assets/icon.png" width="60" height="60"> Baca Novel
#### JCC 2021 Final Project
*Proyek Akhir Bootcamp React Native*

## Deskripsi Aplikasi
Aplikasi ini adalah platform untuk membaca web novel. Aplikasi dapat menampilkan daftar novel terbaru, terfavorit, dan pencarian dari pengguna. Pengguna dapat menambahkan novel ke daftar favorit agar tidak terlupa di kemudian hari. Riwayat novel yang telah dibaca juga tersimpan pada koleksi pengguna.

## Sumber Data
- API :[https://api-buku.online.masuk.id/](https://api-buku.online.masuk.id/)
- Konten novel :[https://www.wattpad.com/](https://www.wattpad.com/)
